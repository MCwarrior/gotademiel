<?php
require_once "autoloader.php";

class Usuario
{

    const EXITO = 1;
    const ERROR = -1;
    const CORREO_DUPLICADO = -2;

    public $id;
    public $nombre;
    public $correo;
    public $contrasenia;
    public $fechaNacimiento;
    public $tipoUsuario;
    public $codigoVerificacion;
    public $fechaHoraVerificacion;
    public $verificado;
    
    public $direccion;
    public $telefono;
    
    public function preregistrar()
    {
        $resultado = self::EXITO;
        
        $conexion = Bd::obtenerConexion();

        $this->codigoVerificacion = self::obtenerCodigoUnico();

        $sql = "INSERT INTO USUARIOS (NOMBRE, CORREO, CONTRASENIA, FECHA_NACIMIENTO, TIPO_USUARIO, CODIGO_VERIFICACION, FECHA_HORA_VERIFICACION, DIRECCION, TELEFONO) "
            . "VALUES(?, ?, ?, ?, 'cliente', ?, NOW(), ?, ?)";
        $stmt = $conexion->prepare($sql);
        $contrasenia = password_hash($this->contrasenia, PASSWORD_DEFAULT);
        $stmt->bind_param(
            "sssssss", 
            $this->nombre, 
            $this->correo, 
            $contrasenia, 
            $this->fechaNacimiento, 
            $this->codigoVerificacion, 
            $this->direccion,
            $this->telefono
        );

        if(!$stmt->execute()){
            $resultado = self::ERROR;
            if($conexion->errno == 1062){  
                $resultado = self::CORREO_DUPLICADO;                
            }
        }        
        return $resultado;     
    }  

    function __construct()
    { }

    public static function obtenerCodigoUnico()
    {
        $conexion = Bd::obtenerConexion();
        do {
            $codigo = md5(uniqid(rand(), true));

            $sql = "SELECT CORREO FROM USUARIOS WHERE CODIGO_VERIFICACION='$codigo'";

            $resultado = $conexion->query($sql);

            if ($resultado->num_rows == 0)
                return $codigo;
        } while (true);
    }    

    public static function obtenerUsuario($correo)
    {

        $usuario = new Usuario;

        $sql = "SELECT id, nombre, correo, fecha_nacimiento, tipo_usuario, codigo_verificacion, 
            fecha_hora_verificacion, verificado, direccion, telefono FROM USUARIOS WHERE CORREO=?";            

        $conexion = Bd::obtenerConexion();

        $stmt = $conexion->prepare($sql);      

        $stmt->bind_param("s", $correo);
        $stmt->bind_result(
            $usuario->id,
            $usuario->nombre,
            $usuario->correo,
            $usuario->fechaNacimiento,
            $usuario->tipoUsuario,
            $usuario->codigoVerificacion,
            $usuario->fechaHoraVerificacion,
            $usuario->verificacion,
            $usuario->direccion,
            $usuario->telefono
        );

        $stmt->execute();
        $huboUsuario = $stmt->fetch();
        $stmt->close();
        if (!$huboUsuario) {
            return self::USUARIO_NO_ENCONTRADO;
        } else {
            return $usuario;
        }
    }

    public function iniciarSesion(){
        if(session_status() != PHP_SESSION_ACTIVE)
            session_start();
        $_SESSION["id"] = $this->id;
        $_SESSION["correo"] = $this->correo;
        $_SESSION["nombre"] = $this->nombre;
        $_SESSION["tipo_usuario"] = $this->tipoUsuario;
        $_SESSION["direccion"] = $this->direccion;
    }

    public static function cerrarSesion(){
        if(session_status() != PHP_SESSION_ACTIVE)
            session_start();
        unset($_SESSION["id"]);
        unset($_SESSION["correo"]);
        unset($_SESSION["nombre"]);
        unset($_SESSION["tipo_usuario"]);
    }

    public static function borrarUsuario($correo)
    {
        $conexion = Bd::obtenerConexion();
        $stmt = $conexion->prepare("DELETE FROM usuarios WHERE correo = ?");
        $stmt->bind_param('s', $correo);
        $stmt->execute();
        $stmt->close();
    }
}