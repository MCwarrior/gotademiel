<?php

class Mensajes
{
    public static function obtenerMensaje($tipo)
    {
        if (session_status() != PHP_SESSION_ACTIVE)
            session_start();

        if (isset($_SESSION[$tipo])) {
            $mensaje = $_SESSION[$tipo];
            unset($_SESSION[$tipo]);
            return $mensaje;
        }
    }

    public static function establecerMensaje($tipo, $mensaje)
    {
        if (session_status() != PHP_SESSION_ACTIVE)
            session_start();

        $_SESSION[$tipo] = $mensaje;                
    }
}
