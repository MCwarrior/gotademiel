<?php
require_once "class.phpmailer.php";
require_once "class.smtp.php";

function enviarCorreo($correoDestinatario, $nombreDestinatario, $asunto, $cuerpo)
{
    $config = parse_ini_file('config.ini');

    $mail = new PHPMailer;
    $mail->isSMTP();
    $mail->CharSet = "UTF-8";
    //Enable SMTP debugging
    // 0 = off (for production use)
    // 1 = client messages
    // 2 = client and server messages
    $mail->SMTPDebug = 0;
    $mail->Debugoutput = 'html';
    $mail->Host = $config["servidorcorreo"];
    $mail->Port = $config["puertocorreo"];
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = $config["seguridadcorreo"];
    $mail->Username = $config["usuariocorreo"];
    $mail->Password = $config["contraseniacorreo"];
    $mail->setFrom($config["usuariocorreo"], 'No responder');
    $mail->addAddress($correoDestinatario, $nombreDestinatario);
    $mail->Subject = $asunto;
    $mail->IsHTML(true);
    $mail->Body = $cuerpo;

    if (!$mail->send()) {
        return false;
    }
    return true;
}
