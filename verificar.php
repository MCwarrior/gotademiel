<?php

require_once "privado/autoloader.php";
require_once "privado/Usuario.php";

if (isset($_GET["cadena_verificacion"])) { 
    $resultado = Usuario::EXITO;
    
    $conexion = Bd::obtenerConexion();

    $sql = "SELECT ID, VERIFICADO FROM USUARIOS WHERE CODIGO_VERIFICACION=?";
    $stmt = $conexion->prepare($sql);
    $stmt->bind_param("s", $_GET["cadena_verificacion"]);
    $stmt->bind_result($id, $verificado);

    if(!$stmt->execute()){
        $resultado = Usuario::ERROR;
        return $resultado;
    }    

    if($stmt->fetch()){
        $stmt->close();
        if($verificado){
            Mensajes::establecerMensaje("aviso", "Tu correo ya fue verificado, puedes iniciar sesión");
            header("location: login.php");
        }else{
            $sql = "UPDATE USUARIOS SET VERIFICADO=TRUE WHERE CODIGO_VERIFICACION=?";
            $stmt = $conexion->prepare($sql);
            $stmt->bind_param("s", $_GET["cadena_verificacion"]);
            if(!$stmt->execute())
                return Usuario::ERROR;
            Mensajes::establecerMensaje("aviso", "Tu correo ya fue verificado, puedes iniciar sesión");
            header("location: login.php");
        }
    }
} else{
    Mensajes::establecerMensaje("error", "Parece que te faltan datos para verificar el correo");
    header("location: registro.php");
}
