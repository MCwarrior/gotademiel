<?php

require_once "privado/autoloader.php";

if (session_status() != PHP_SESSION_ACTIVE)
    session_start();

if (!isset($_SESSION['id'])) {
    Mensajes::establecerMensaje("error", "Necesitas iniciar sesión");
    header("location: login.php");
    exit;
}

if ($_SESSION['tipo_usuario'] != 'cliente') {
    Mensajes::establecerMensaje("error", "Necesitas ser cliente");
    header("location: index.php");
    exit;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>La gota de miel</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/estilos.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
</head>

<body>
    <header>
        <img id="logoHeader" src="img/logo.png">
        <h1 id="tituloHeader">La Gota De Miel</h1>
        <div id="botonesHeader">
            <?php
            if (session_status() != PHP_SESSION_ACTIVE)
                session_start();
            if (isset($_SESSION["id"])) {
                print("Bienvenido " . $_SESSION["nombre"] . " |");
                print("<a href='perfil.php' id='headerMiPerfil'>Mi Perfil</a> |");
                print("<a href='logout.php' id='headerMiPerfil'>Cerrar sesión</a> |");
            } else {
                print("<a href='login.php' id='headerLogin'>Iniciar sesión</a> | <a href='registro.php' id='indexRegistro'>Registrarse</a>");
            }
            ?>
        </div>
        <div class="scrollmenu" id="menuHeader">
            <a href="index.php">Inicio</a>
            <a href="categorias.php">Categorías</a>
            <a href="listaProductos.php">Productos</a>
            <a href="#about">Contacto</a>
            <a href="carrito.php">Carrito</a>
            <a href="pedidos.php">Pedidos</a>
        </div>
    </header>
    <main>
        <h1>Mis Pedidos</h1>
        <table id="misPedidos">
            <tr>
                <th>Folio</th>
                <th>Fecha</th>
                <th>Estado</th>
                <th>Total</th>
            </tr>
            <?php

            $conexion = Bd::obtenerConexion();
            $sql = "SELECT folio, fecha, estado, total from pedidos where id_usuario=" . $_SESSION['id'];

            $res = $conexion->query($sql);

            while ($datos = $res->fetch_assoc()) {
                ?>
                <tr>
                    <td><?=$datos['folio']?></td>
                    <td><?=$datos['fecha']?></td>
                    <td><?=$datos['estado']?></td>
                    <td><?=$datos['total']?></td>
                </tr>
            <?php
            }
            ?>
        </table>
    </main>
    <footer>
        Av. 20 de noviembre #51 (627) 522-1217 || Av. independencia #85 (627) 523-3520
    </footer>
</body>

</html>