<?php
if (session_status() != PHP_SESSION_ACTIVE)
    session_start();

if(!isset($_POST['codigo'])){
    echo false;
    exit;
}

if(!isset($_SESSION['cart'])){
    $_SESSION['cart'] = [];
}

if(!isset($_SESSION['cart'][$_POST['codigo']])){
    $_SESSION['cart'][$_POST['codigo']] = 1;    
}else{
    $_SESSION['cart'][$_POST['codigo']]++;
}

echo $_SESSION['cart'][$_POST['codigo']];
