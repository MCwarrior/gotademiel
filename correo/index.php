<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            main{
                width: 500px;
                margin: auto;
            }
            button{
                width: 250px;
                height: 50px;
                background-color: #33ccff;
                color: white;
                border-style: none;
                border-radius: 3px;
                margin: 5px;
                
            }
        </style>
    </head>
    <body>        
        <main>
            <h1>Correo Electrónico</h1>
            <h3>¿Cómo es posible enviar un correo electrónico vía PHP?</h3>
            <p>Usando la librería PHPMailer, a la cual se le especifican los 
            parámetros de método, servidor smtp, usuario, contraseña y 
            destinatarios.</p>
            <h3>¿Cómo es posible leer un correo electrónico vía PHP?</h3>
            <p>Se hace uso de la función imap_open para abrir la conexion
            al servidor IMAP, el cual proveerá el listado de correos según
            los parámetros que se especifique en la consulta.</p>
            <button type="button" id="enviar">Enviar correo</button><br>
            <button type="button" id="leer">Leer correos</button>
        </main>
        <script>
            document.querySelector("#enviar").onclick = function(){
                window.location.href = "enviar.html";
            }
            document.querySelector("#leer").onclick = function(){
                window.location.href = "leer.php";
            }
        </script>
    </body>
</html>
