<?php
// En versiones de PHP anteriores a la 4.1.0, debería utilizarse $HTTP_POST_FILES en lugar
// de $_FILES.

require_once "privado/autoloader.php";

if (!isset($_POST['codigo'])) {
    Mensajes::establecerMensaje("error", "Parece que te faltan datos");
    header("location: administrarProductos.php");
    exit;
}

$conexion = Bd::obtenerConexion();

$dest = "productos/imagen-" . $_POST['codigo'] . ".jpg";
if (move_uploaded_file($_FILES['imagen']['tmp_name'], $dest)) {
    $sql = "UPDATE productos set imagen='productos/imagen-" . $_POST['codigo'] . ".jpg'"
        . "where codigo=" . $_POST['codigo'];

    if ($conexion->query($sql)) {
        Mensajes::establecerMensaje("aviso", "La imagen se cargó exitosamente");
        header("location: administrarProductos.php");
        exit;
    }
}

Mensajes::establecerMensaje("error", "Hubo un problema al cargar La imagen, intenta de nuevo " . $_FILES['imagen']['error']);
header("location: administrarProductos.php");
exit;