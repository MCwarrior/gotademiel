<?php

require_once "privado/autoloader.php";

if (session_status() != PHP_SESSION_ACTIVE)
    session_start();

$conexion = Bd::obtenerConexion();

$sql = "INSERT into pedidos(id_usuario, total, direccion) "
    . "values(" . $_SESSION['id'] . ", " . $_SESSION['total']
    . ",'" . $_POST['direccion'] . "')";
$folio = 0; 

mysqli_autocommit($conexion, false);
if ($conexion->query($sql)) {
    $sql = "SELECT max(folio) as folio from pedidos";
    $res = $conexion->query($sql);
    $folio = $res->fetch_assoc()['folio'];
    for ($i = 0; $i < sizeof($_SESSION['codigo']); $i++) {
        $sql = "INSERT into pedidos_productos(folio_pedido, codigo_producto, cantidad, precio) "
            . "values(" . $folio . "," . $_SESSION['codigo'][$i] . "," . $_SESSION['cantidad'][$i] . ","
            . $_SESSION['precio'][$i] . ")";
        if (!$conexion->query($sql)) {
            mysqli_rollback($conexion);
            mysqli_autocommit($conexion, true);
            limpiarCarrito();
            echo false;
            exit;
        }
    }

    mysqli_commit($conexion);
    mysqli_autocommit($conexion, true);
    echo true;

    $server = $_SERVER["HTTP_HOST"];
    if ($server == "localhost") {
        $server = $server . "/gotademiel";
    }
    $asunto = "La Gota De Miel | Pedido registrado";
    $cuerpo = "<h1>Se registró correctamente tu pedido</h1></br></br>"
        . "Tu pedido se registró bajo el folio " . $folio . ". Te mantendremos al tanto de tu pedido mediante este email, te recomendamos revisarlo 
        constantemente. Si tienes alguna duda respecto a tu pedido estamos a tus órdenes en el 
        telefono (123)456-7890.<br><br>
        <table>
        <tr>
            <th>Código</th>
            <th>Producto</th>
            <th>Precio</th>
            <th>Cantidad</th>
        </tr>";

        for ($i = 0; $i < sizeof($_SESSION['codigo']); $i++) {
            $cuerpo = $cuerpo . "<tr>"
                    . "<td>" . $_SESSION['codigo'][$i] . "</td>"
                    . "<td>" . $_SESSION['nombreProd'][$i] . "</td>"
                    . "<td>" . $_SESSION['precio'][$i] . "</td>"
                    . "<td>" . $_SESSION['cantidad'][$i] . "</td>"
                    . "</tr>";
        }

        $cuerpo = $cuerpo . "<br><br><h2>Total:$" . $_SESSION['total'] . "</h2>";


    enviarCorreo($_SESSION['correo'], $_SESSION['nombre'], $asunto, $cuerpo);
    limpiarCarrito();
    
}

function limpiarCarrito()
{
    unset($_SESSION['cart']);
    unset($_SESSION['codigo']);
    unset($_SESSION['cantidad']);
    unset($_SESSION['precio']);
    unset($_SESSION['total']);    
    unset($_SESSION['cart']);
}
