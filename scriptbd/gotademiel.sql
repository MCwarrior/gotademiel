-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: gotademiel
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pedidos`
--

DROP TABLE IF EXISTS `pedidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedidos` (
  `folio` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT CURRENT_TIMESTAMP,
  `total` float NOT NULL,
  `estado` enum('aceptado','enviado','recibido','cancelado') DEFAULT 'aceptado',
  `direccion` varchar(511) NOT NULL,
  PRIMARY KEY (`folio`),
  KEY `id_usuario` (`id_usuario`),
  CONSTRAINT `pedidos_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedidos`
--

LOCK TABLES `pedidos` WRITE;
/*!40000 ALTER TABLE `pedidos` DISABLE KEYS */;
INSERT INTO `pedidos` VALUES (1,1033,'2019-06-10 08:02:11',190,'enviado','Cedro 28, Benito Juárez'),(2,1033,'2019-06-10 08:05:51',190,'aceptado','Cedro 28, Benito Juárez'),(3,1033,'2019-06-10 08:10:49',190,'aceptado','Cedro 28, Benito Juárez'),(4,1033,'2019-06-10 08:20:33',110,'aceptado','Cedro 28, Benito Juárez'),(5,1034,'2019-06-10 13:48:46',65,'aceptado','cedro 28'),(6,1037,'2019-06-10 14:08:35',280000,'aceptado','cedro 28');
/*!40000 ALTER TABLE `pedidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedidos_productos`
--

DROP TABLE IF EXISTS `pedidos_productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedidos_productos` (
  `folio_pedido` int(11) NOT NULL,
  `codigo_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` float NOT NULL,
  PRIMARY KEY (`folio_pedido`,`codigo_producto`),
  KEY `codigo_producto` (`codigo_producto`),
  CONSTRAINT `pedidos_productos_ibfk_1` FOREIGN KEY (`folio_pedido`) REFERENCES `pedidos` (`folio`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pedidos_productos_ibfk_2` FOREIGN KEY (`codigo_producto`) REFERENCES `productos` (`codigo`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedidos_productos`
--

LOCK TABLES `pedidos_productos` WRITE;
/*!40000 ALTER TABLE `pedidos_productos` DISABLE KEYS */;
INSERT INTO `pedidos_productos` VALUES (1,1,5,5),(1,2,3,55),(2,1,5,5),(2,2,3,55),(3,1,5,5),(3,2,3,55),(4,2,2,55),(5,1,2,5),(5,2,1,55),(6,1,1000,5),(6,2,5000,55);
/*!40000 ALTER TABLE `pedidos_productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productos`
--

DROP TABLE IF EXISTS `productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productos` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `categoria` enum('dulcesdeleche','chocolates','garapiñados','otros') DEFAULT 'otros',
  `imagen` varchar(100) DEFAULT NULL,
  `precio` float DEFAULT NULL,
  `stock` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos`
--

LOCK TABLES `productos` WRITE;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` VALUES (1,'Varquillo','dulcesdeleche','productos/imagen-1.jpg',5,100),(2,'Chocolates Queen Anne','chocolates','productos/queenanne.jpeg',55,0),(3,'Chu','chocolates','productos/varquillo.jpg',2,2),(4,'Chu','chocolates','productos/varquillo.jpg',2,2);
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `correo` varchar(63) NOT NULL,
  `contrasenia` varchar(512) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `tipo_usuario` enum('admin','cliente') DEFAULT NULL,
  `codigo_verificacion` varchar(63) DEFAULT NULL,
  `fecha_hora_verificacion` datetime DEFAULT NULL,
  `verificado` tinyint(4) DEFAULT '0',
  `direccion` varchar(100) NOT NULL,
  `telefono` varchar(12) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `correo` (`correo`)
) ENGINE=InnoDB AUTO_INCREMENT=1038 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1033,'Raul','Mcwarrior.mendez@gmail.com','$2y$10$qqEP6nYdj1uWZeF7kSCGz.xNru7VaRKSTF99gUgEjvfrnPn0rZzPm','1998-03-26','admin','6ad2579db8b727092cdcb71b2c9349c0','2019-05-26 20:53:08',1,'Cedro 28, Benito Juárez','6271338095'),(1034,'luis adrian','adrian.navarro23@hotmail.com','$2y$10$UGy51DQCmqNcZWNFxmy3Runtcqwp6PdBqS8pyshU2D9S.G9e9nGR6','1997-07-31','admin','987d6defc50a7ff8b2a78501f2d04df1','2019-06-10 13:43:06',1,'cedro 28','6271516191'),(1037,'\'','omar@itparral.edu.mx','$2y$10$.Ysrs8o1awcer/Kpe5mYl.4rY4jDFcWQd5vvqOXKjRpGw9rjVl6lq','5119-06-13','cliente','bd45e393c198d5bdb3b4e34b6896153f','2019-06-10 14:05:36',1,'cedro 28','6271516191');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-14 10:29:33
