<?php
require_once "privado/autoloader.php";
require_once "privado/Usuario.php";

if (
    isset($_POST["nombre"]) && isset($_POST["correo"]) && isset($_POST["contrasenia"]) && isset($_POST["fechaNacimiento"])
    && isset($_POST["direccion"]) && isset($_POST["telefono"])
) {

    do {
        $usuario = new Usuario();

        $usuario->nombre = $_POST["nombre"];
        $usuario->correo = $_POST["correo"];
        $usuario->contrasenia = $_POST["contrasenia"];
        $usuario->fechaNacimiento = $_POST["fechaNacimiento"];
        $usuario->direccion = $_POST["direccion"];
        $usuario->telefono = $_POST["telefono"];

        $respuesta = $usuario->preregistrar();

        if ($respuesta == Usuario::CORREO_DUPLICADO) {
            $usuario = Usuario::obtenerUsuario($_POST["correo"]);
            if ($usuario->verificado) {
                Mensajes::establecerMensaje($error, "El usuario con correo " . $_POST['correo'] . "ya está registrado y verificado");
                header("location: login.php");
            } else {
                Usuario::borrarUsuario($usuario->correo);
            }
        } else {
            break;
        }
    } while (true);

    switch ($respuesta) {
        case Usuario::ERROR:
            Mensajes::establecerMensaje("error", "Sucedió un error.</br>Inténtalo más tarde.");
            header("Location: registro.php");
            break;
        case Usuario::EXITO:

            $server = $_SERVER["HTTP_HOST"];
            if ($server == "localhost") {
                $server = $server . "/gotademiel";
            }
            $asunto = "Estás a un paso de completar el registro";
            $cuerpo = "Tu preregistro a nuestro sitio ha sido exitoso.</br></br>"
                . "Para finalizar debemos de activar tú cuenta, favor de ingresar al siguiente enlace:</br>"
                . "<a href='http://$server/verificar.php?cadena_verificacion=$usuario->codigoVerificacion'>"
                . "http://$server/verificar.php?cadena_verificacion=$usuario->codigoVerificacion</a>";

            if (enviarCorreo($usuario->correo, 'Nuevo usuario preregistrado', $asunto, $cuerpo)) {
                Mensajes::establecerMensaje("aviso", "Estás a un paso de completar el registro.</br>Se envío a tu correo electrónico un enlace para confirmar tu cuenta.");
                header("Location: login.php");
            } else {
                Mensajes::establecerMensaje("error", "Sucedió un error al enviar el correo de verificación.</br>Intenta el registro de nuevo.");
                header("Location: registro.php");
            }
            break;
        case Usuario::CORREO_DUPLICADO:
            Mensajes::establecerMensaje("error", "Tu correo ya se encuentra registrado");
            header("location: login.php");
            break;
        default:
            echo "...";
            exit;
            break;
    }
} else {
    Mensajes::establecerMensaje("error", "Ingresaste los datos incompletos");
    header("location: registro.php");
}
