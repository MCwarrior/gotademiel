<?php

require_once "privado/autoloader.php";

if(session_status() != PHP_SESSION_ACTIVE)
    session_start();

if(!isset($_SESSION['id'])){
    Mensajes::establecerMensaje("error", "Necesitas inicias sesión");
    header("location: login.php");
    exit;
}else if($_SESSION['tipo_usuario'] == 'cliente')
    header("location: mispedidos.php");
else
    header("location: administrarPedidos.php");