<!DOCTYPE html>
<html lang="en">

<?php

require_once "privado/autoloader.php";

if (session_status() != PHP_SESSION_ACTIVE)
    session_start();

if (!isset($_SESSION['id']) || !isset($_POST['cantidad']) || !isset($_POST['precio']) || !isset($_POST['total']) || !isset($_POST['codigo']) || !isset($_POST['nombreProd'])) {
    Mensajes::establecerMensaje("error", "Creo que te faltan datos");
    header("location: carrito.php");
    exit;
}

$_SESSION['total'] = $_POST['total'];
$_SESSION['cantidad'] = $_POST['cantidad'];
$_SESSION['precio'] = $_POST['precio'];
$_SESSION['total'] = $_POST['total'];
$_SESSION['codigo'] = $_POST['codigo'];
$_SESSION['nombreProd'] = $_POST['nombreProd'];

echo sizeof($_SESSION['codigo']);

$conexion = Bd::obtenerConexion();
?>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>La gota de miel</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/estilos.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
</head>

<body>
    <header>
        <img id="logoHeader" src="img/logo.png">
        <h1 id="tituloHeader">La Gota De Miel</h1>
        <div id="botonesHeader">
            <?php
            if (session_status() != PHP_SESSION_ACTIVE)
                session_start();
            if (isset($_SESSION["id"])) {
                print("Bienvenido " . $_SESSION["nombre"] . " |");
                print("<a href='perfil.php' id='headerMiPerfil'>Mi Perfil</a> |");
                print("<a href='logout.php' id='headerMiPerfil'>Cerrar sesión</a> |");
            } else {
                print("<a href='login.php' id='headerLogin'>Iniciar sesión</a> | <a href='registro.php' id='indexRegistro'>Registrarse</a>");
            }
            ?>
        </div>
        <div class="scrollmenu" id="menuHeader">
            <a href="index.php">Inicio</a>
            <a href="categorias.php">Categorías</a>
            <a href="listaProductos.php">Productos</a>
            <a href="#about">Contacto</a>
            <a href="carrito.php">Carrito</a>
        </div>
    </header>
    <main>
        <div id="carrito">
            <form action="" method="post">
                <h1>Envío</h1>
                <div id="total">$<span><?= $_SESSION['total'] ?></span>MXN</div>
                Dirección de envío:<br>
                <textarea name="direccion"><?= $_SESSION['direccion'] ?></textarea>
                <script src="https://www.paypal.com/sdk/js?client-id=ATZvKEcazLLcFcEv9Bk5xhOrHTbPKl6j0qVCn7LvBry7ImxYS9YVDyckU8W7HvrGk1v6XyLE15RZ1hvG&currency=MXN">
                </script>
                <div id="paypal-button-container"></div>
                <script>
                    paypal.Buttons({
                        createOrder: function(data, actions) {
                            // Set up the transaction                            
                            return actions.order.create({
                                purchase_units: [{
                                    amount: {
                                        value: $("#total>span").text(),

                                    }
                                }]
                            });
                        },
                        onApprove: function(data, actions) {
                            // Capture the funds from the transaction
                            return actions.order.capture().then(function(payment) {
                                // Show a success message to your buyer                        
                                $.post(
                                    "registrarPedido.php",
                                    {direccion: $("textarea[name='direccion']").text()},
                                    function(data, status){
                                        if(data){
                                            alert("Tu pedido fue registrado. En tu correo encontrarás mas información"
                                            + "Gracias por tu compra :)");
                                            location.href="index.php";
                                        }else{
                                            alert("Hubo un problema :( por favor contacta al administrador");
                                            location.href="index.php";
                                        }
                                    }
                                );
                            });
                        }
                    }).render('#paypal-button-container');
                    $("#paypal-button-container").children().width('75%');
                    $("#paypal-button-container").children().css('margin: auto');
                </script>
            </form>
        </div>
    </main>
    <footer>
        Av. 20 de noviembre #51 (627) 522-1217 || Av. independencia #85 (627) 523-3520
    </footer>
    <script>

    </script>
</body>

</html>