<?php
    require_once "privado/autoloader.php";
    $error = Mensajes::obtenerMensaje("error");
    $aviso = Mensajes::obtenerMensaje("aviso");
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>La gota de miel</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/estilos.css">
</head>
<body>
    <div class="logo">
        <a href="index.php"> <img src="img/logo.png" alt="Logo"></a>
    </div>
    <form action="procesarLogin.php" method="POST" id="login">
        <h1>Iniciar Sesión</h1>
        <?php                           
            if(isset($error)){
                echo "<div class='alert alert-danger' role='alert'>$error</div>";                
            }
            if(isset($aviso)){
                echo "<div class='alert alert-success' role='alert'>$aviso</div>";                
            }
        ?>        
        <label for="correo">Correo</label>
        <input type="email" name="correo">
        <label for="contrasenia">Contraseña</label>
        <input type="password" name="contrasenia">    
        <a href="registro.php">Registrarse</a> 
        <input type="submit" value="Acceder" id="btnAcceder">
    </form>
</body>
</html>