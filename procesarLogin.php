<?php

require_once "privado/autoloader.php";
require_once "privado/Usuario.php";


if(isset($_POST["correo"]) && isset($_POST["contrasenia"])){
    $conexion = Bd::obtenerConexion();
    
    $sql = "SELECT correo, contrasenia, verificado FROM usuarios WHERE correo=?";

    $stmt = $conexion->prepare($sql);
    $stmt->bind_param("s", $_POST["correo"]);
    $stmt->bind_result($correo, $contrasenia, $verificado);        
    if(!$stmt->execute()){
        $stmt->close();
        Mensajes::establecerMensaje("error", "Hubo un problema");
        header("location: login.php");        
        exit;
    }

    if(!$stmt->fetch()){        
        $stmt->close();
        Mensajes::establecerMensaje("error", "El correo no está registrado");
        header("location: login.php");
        exit;
    }

    $stmt->close();

    if(!$verificado){
        Mensajes::establecerMensaje("error", "El correo no está verificado");
        header("location: login.php");
        exit;
    }

    if(password_verify($_POST["contrasenia"], $contrasenia)){
        $usuario = Usuario::obtenerUsuario($_POST["correo"]);        
        $usuario->iniciarSesion();        
        header("location: index.php");
        exit;
    }else{
        Mensajes::establecerMensaje("error", "Contraseña incorrecta");
        header("location: login.php");
        exit;
    }

}else{
    Mensajes::establecerMensaje("error", "Parece que te faltan datos");
    header("location: login.php");
}
