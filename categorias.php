<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>La gota de miel</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/estilos.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
</head>

<body>
    <header>
        <img id="logoHeader" src="img/logo.png">
        <h1 id="tituloHeader">La Gota De Miel</h1>
        <div id="botonesHeader">
            <?php
            if (session_status() != PHP_SESSION_ACTIVE)
                session_start();
            if (isset($_SESSION["id"])) {
                print("Bienvenido " . $_SESSION["nombre"] . " |");
                print("<a href='perfil.php' id='headerMiPerfil'>Mi Perfil</a> |");
                print("<a href='logout.php' id='headerMiPerfil'>Cerrar sesión</a> |");
            } else {
                print("<a href='login.php' id='headerLogin'>Iniciar sesión</a> | <a href='registro.php' id='indexRegistro'>Registrarse</a>");
            }
            ?>
        </div>
        <div class="scrollmenu" id="menuHeader">
            <a href="index.php">Inicio</a>
            <a href="categorias.php">Categorías</a>
            <a href="listaProductos.php">Productos</a>
            <a href="#about">Contacto</a>
            <a href="carrito.php">Carrito</a>
            <?php if (isset($_SESSION['tipo_usuario'])){
                if($_SESSION['tipo_usuario'] == 'admin') echo "<a href='administrarProductos.php'>Admin. Productos</a>"; 
                
            }?>
        </div>
    </header>
    <main>
        <h1>Categorías</h1>
        <a href="listaProductos.php?categoria=dulcesdeleche">
            <div id="catDulcesDeLeche" class="contenedoresPrincipales">
                <img src="img/dulcesdeleche.jpg" alt=""><br>
                <h3>Dulces De Leche</h3>
            </div>
        </a>
        <a href="listaProductos.php?categoria=chocolates">
            <div id="catChocolates" class="contenedoresPrincipales">
                <img src="img/chocolates.jpg" alt=""><br>
                <h3>Chocolates</h3>
            </div>
        </a>
        <a href="listaProductos.php?categoria=garapiñados">
            <div id="catGarapiñados" class="contenedoresPrincipales">
                <img src="img/garapiñados.jpg" alt=""><br>
                <h3>Garapiñados</h3>
            </div>
        </a>
        <a href="listaProductos.php?categoria=otros">
            <div id="catOtros" class="contenedoresPrincipales">
                <img src="img/otrosDulces.jpg" alt=""><br>
                <h3>Otros</h3>
            </div>
        </a>
    </main>
    <footer>
        Av. 20 de noviembre #51 (627) 522-1217 || Av. independencia #85 (627) 523-3520
    </footer>
</body>

</html>