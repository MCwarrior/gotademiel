<!DOCTYPE html>
<html lang="en">

<?php

require_once "privado/autoloader.php";

if (session_status() != PHP_SESSION_ACTIVE)
    session_start();

if (!isset($_SESSION['id'])) {
    header("location: index.php");
    exit;
}

$error = Mensajes::obtenerMensaje("error");
$aviso = Mensajes::obtenerMensaje("aviso");

$conexion = Bd::obtenerConexion();
?>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>La gota de miel</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/estilos.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
</head>

<body>
    <header>
        <img id="logoHeader" src="img/logo.png">
        <h1 id="tituloHeader">La Gota De Miel</h1>
        <div id="botonesHeader">
            <?php
            if (session_status() != PHP_SESSION_ACTIVE)
                session_start();
            if (isset($_SESSION["id"])) {
                print("Bienvenido " . $_SESSION["nombre"] . " |");
                print("<a href='perfil.php' id='headerMiPerfil'>Mi Perfil</a> |");
                print("<a href='logout.php' id='headerMiPerfil'>Cerrar sesión</a> |");
            } else {
                print("<a href='login.php' id='headerLogin'>Iniciar sesión</a> | <a href='registro.php' id='indexRegistro'>Registrarse</a>");
            }
            ?>
        </div>
        <div class="scrollmenu" id="menuHeader">
            <a href="index.php">Inicio</a>
            <a href="categorias.php">Categorías</a>
            <a href="listaProductos.php">Productos</a>
            <a href="#about">Contacto</a>
            <a href="carrito.php">Carrito</a>
        </div>
    </header>
    <main>
        <div id="carrito">
            <form action="envio.php" method="post">
                <h1>Carrito</h1>
                <?php
                if (isset($error)) {
                    echo "<div class='alert alert-danger' role='alert'>$error</div>";
                }
                if (isset($aviso)) {
                    echo "<div class='alert alert-success' role='alert'>$aviso</div>";
                }
                if(!isset($_SESSION['cart'])){
                    print(" Tu carrito está vacío \n <a href=index.php> Volver al inicio </a>");
                    exit;
                }
                ?>
                
                <table>
                    <tr>
                        <th>Código</th>
                        <th>Producto</th>
                        <th>Precio</th>
                        <th>Cantidad</th>
                    </tr>
                    <?php
                    $total = 0;
                    
                    foreach ($_SESSION['cart'] as $codigo => $cantidad) {
                        $sql = "SELECT nombre, precio from productos where codigo=$codigo";
                        $res = $conexion->query($sql);
                        $valores = $res->fetch_assoc();

                        ?>
                        
                        <tr>
                            <td><input type="hidden" name="codigo[]" value="<?= $codigo ?>"><?= $codigo ?></td>
                            <td><input type="hidden" name="nombreProd[]" value="<?= $valores['nombre'] ?>"><?= $valores['nombre'] ?></td>
                            <td><input type="hidden" name="precio[]" value="<?= $valores['precio'] ?>" class="precio"><?= $valores['precio'] ?></td>
                            <td><input type="number" min="0" name="cantidad[]" value="<?= $cantidad ?>" class="cantidad"></td>
                        </tr>

                        <?php
                        $total += $valores['precio'] * $cantidad;
                    }
                    ?>
                </table>
                <div id="total">$<span><?= $total ?></span>MXN</div>
                <input type="hidden" name="total" value="<?= $total ?>">
                <button type="submit" class="btn btn-success">Siguiente</button>
            </form>
        </div>
    </main>
    <footer>
        Av. 20 de noviembre #51 (627) 522-1217 || Av. independencia #85 (627) 523-3520
    </footer>
    <script>
        $(".cantidad").change(function() {
            $precios = $(".precio");
            $cantidad = $(".cantidad");
            $total = 0;
            for (i = 0; i < $precios.length; i++) {
                $total += $precios[i].value * $cantidad[i].value;
            }

            $("#total>span").text($total);
            $("input[name='total']").val($total);
        });
    </script>
</body>

</html>