<?php

require_once "privado/autoloader.php";

if (!isset($_POST['folio']) || !isset($_POST['estado'])) {
    echo false;
    exit;
}

$conexion = Bd::obtenerConexion();

$sql = "UPDATE pedidos set estado='" . $_POST['estado'] . "' where folio=" . $_POST['folio'];

if ($res = $conexion->query($sql)) {
    $sql = "SELECT correo, nombre from usuarios inner join pedidos on usuarios.id=pedidos.id_usuario where pedidos.folio=" . $_POST['folio'];    
    $res = $conexion->query($sql);    
    $datos = $res->fetch_assoc();

    $correo = $datos['correo'];
    $nombre = $datos['nombre'];

    $server = $_SERVER["HTTP_HOST"];
    if ($server == "localhost") {
        $server = $server . "/gotademiel";
    }
    $asunto = "La Gota De Miel | Pedido actualizado";
    $cuerpo = "<h1>La Gota De Miel | Pedido #" . $_POST['folio'] . " actualizado</h1></br></br>"
        . "El estado de tu pedido ha sido actualizado, ahora se encuentra " . $_POST['estado'] . ". " 
        . "Si tienes alguna duda comunícate a alguna de nuestras sucursales<br><br>"
        . "<a href=http://itpbooks.me>La Gota De Miel</a>";

    enviarCorreo($correo, $nombre, $asunto, $cuerpo);
    echo $correo;
    exit;
}
echo false;

