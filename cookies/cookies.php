<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <h1>Ejemplo con cookies</h1>
        <?php
            setcookie("test", " ", time() + 3600);
        ?>
        <?php
            if(count($_COOKIE) == 0){
                echo "<h1>Las cookies se encuentran deshabilitadas</h1>";
            }
        ?>
        Tu nombre es:
        <?php
            if(isset($_COOKIE["nombre"])){
                echo $_COOKIE["nombre"];
            }else{
                echo "No se encuentra definido";
            }
        ?>
        <br>
        Tu apellido es:
        <?php
            if(isset($_COOKIE["apellido"])){
                echo $_COOKIE["apellido"];
            }else{
                echo "No se encuentra definido";
            }
        ?>
        <br><br>
        <form action="anadir.php" method="post">
            Modificar nombre: <input type="text" name="nombre"/><br>
            Modificar apellido: <input type="text" name="apellido"/><br>
            <input type="submit" value="Guardar"/>
        </form>
        <br>
        <form action="borrar.php">
            <input type="submit" value="Borrar datos"/>
        </form>
        <br><br>
        <a href="index.html">Volver al inicio</a>
    </body>
</html>
