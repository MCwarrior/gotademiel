<?php

require_once "privado/autoloader.php";

$error = Mensajes::obtenerMensaje("error");
$aviso = Mensajes::obtenerMensaje("aviso");

if (session_status() != PHP_SESSION_ACTIVE)
    session_start();

if (!isset($_SESSION['id'])) {
    Mensajes::establecerMensaje("error", "Necesitas iniciar sesión");
    header("location: login.php");
    exit;
}

if ($_SESSION['tipo_usuario'] != 'admin') {
    Mensajes::establecerMensaje("error", "Necesitas ser administrador");
    header("location: index.php");
    exit;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>La gota de miel</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/estilos.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
</head>

<body>
    <header>
        <img id="logoHeader" src="img/logo.png">
        <h1 id="tituloHeader">La Gota De Miel</h1>
        <div id="botonesHeader">
            <?php
            if (session_status() != PHP_SESSION_ACTIVE)
                session_start();
            if (isset($_SESSION["id"])) {
                print("Bienvenido " . $_SESSION["nombre"] . " |");
                print("<a href='perfil.php' id='headerMiPerfil'>Mi Perfil</a> |");
                print("<a href='logout.php' id='headerMiPerfil'>Cerrar sesión</a> |");
            } else {
                print("<a href='login.php' id='headerLogin'>Iniciar sesión</a> | <a href='registro.php' id='indexRegistro'>Registrarse</a>");
            }
            ?>
        </div>
        <div class="scrollmenu" id="menuHeader">
            <a href="index.php">Inicio</a>
            <a href="categorias.php">Categorías</a>
            <a href="listaProductos.php">Productos</a>
            <a href="#about">Contacto</a>
            <a href="carrito.php">Carrito</a>
            <a href="pedidos.php">Pedidos</a>
            <?php if(isset ($_SESSION['tipo_usuario'])) {
                if( $_SESSION['tipo_usuario'] ==  'admin') echo "<a href='administrarProductos.php'>Admin. Productos</a>"; 
            }
                ?>
        </div>
    </header>
    <main>
        <?php
        if (isset($error)) {
            echo "<div class='alert alert-danger' role='alert'>$error</div>";
        }
        if (isset($aviso)) {
            echo "<div class='alert alert-success' role='alert'>$aviso</div>";
        }
        ?>

        <h1>Nuevo Producto</h1>
        <form action="nuevoProducto.php" method="post" enctype="multipart/form-data">
            <table id="administrarProductos">
                <tr>
                    <th>Nombre</th>
                    <th>Categoría</th>
                    <th>Imagen</th>
                    <th>Precio</th>
                    <th>Existencia</th>
                    <th>Añadir</th>
                </tr>

                <td>
                    <input type="text" name="nombre" id="">
                </td>
                <td>
                    <select name="categoria">
                        <option value="dulcesdeleche" selected>Dulces de leche</option>
                        <option value="chocolates">Chocolates</option>
                        <option value="garapiñados">Garapiñados</option>
                        <option value="otros">Otros</option>
                    </select>
                </td>
                <td>
                    <input type="hidden" name="MAX_FILE_SIZE" value="500000" />
                    <!-- <input type="file" name="imagen" accept="image/*"> -->
                </td>
                <td>
                    <input type="number" name="precio" value="<?= $datos['precio'] ?>">
                </td>
                <td>
                    <input type="number" name="stock" min="0" value="<?= $datos['stock'] ?>" step="1">
                </td>
                <td><button type="submit">Añadir</button></td>

                </tr>
            </table>
        </form>
        <br><br>
        <h1>Productos</h1>
        <table id="administrarProductos">
            <tr>
                <th>Codigo</th>
                <th>Nombre</th>
                <th>Categoría</th>
                <th>Imagen</th>
                <th>Precio</th>
                <th>Existencia</th>
            </tr>
            <?php

            $conexion = Bd::obtenerConexion();
            $sql = "SELECT * from productos order by codigo asc";

            $res = $conexion->query($sql);

            while ($datos = $res->fetch_assoc()) {
                ?>
                <tr>
                    <td><?= $datos['codigo'] ?></td>
                    <td><?= $datos['nombre'] ?></td>
                    <td>
                        <select><?= $datos['categoria'] ?>
                            <option value="dulcesdeleche" <?php if ($datos['categoria'] == 'dulcesdeleche') echo "selected"; ?>>Dulces de leche</option>
                            <option value="chocolates" <?php if ($datos['categoria'] == 'chocolates') echo "selected"; ?>>Chocolates</option>
                            <option value="garapiñados" <?php if ($datos['categoria'] == 'garapiñados') echo "selected"; ?>>Garapiñados</option>
                            <option value="otros" <?php if ($datos['categoria'] == 'otros') echo "selected"; ?>>Otros</option>
                        </select><br>
                        <button type="button" class="guardarCategoria">Actualizar</button>
                    </td>
                    <td>
                        <img class="imagenMuestra" src="<?= $datos['imagen'] ?>">
                        <form enctype="multipart/form-data" action="nuevaImagen.php" method="post">
                            <input type="hidden" name="MAX_FILE_SIZE" value="500000" />
                            <input type="hidden" name="codigo" value="<?= $datos['codigo'] ?>"><br>
                            <input type="file" name="imagen" accept="image/*">
                            <button type="submit">Cargar</button>
                        </form>
                    </td>
                    <td>
                        <input type="number" name="precio" value="<?= $datos['precio'] ?>"><br>
                        <button type="button" class="guardarPrecio">Actualizar</button>
                    </td>
                    <td>
                        <input type="number" name="stock" min="0" value="<?= $datos['stock'] ?>" step="1"><br>
                        <button type="submit" class="guardarStock">Actualizar</button>
                    </td>
                </tr>
            <?php
        }
        ?>
        </table>
    </main>
    <footer>
        Av. 20 de noviembre #51 (627) 522-1217 || Av. independencia #85 (627) 523-3520
    </footer>
</body>

<script>
    $(".guardarCategoria").click(function() {
        $celda = $(this).parent();
        $codigo = $(this).parent().prev().prev().text();
        $categoria = $(this).prev().prev().val();
        console.log($codigo + " " + $categoria);
        $.post(
            "actualizarCategoria.php", {
                codigo: $codigo,
                categoria: $categoria
            },
            function(data, status) {
                if (!data || data === "") {
                    $celda.prepend("<div role='alert' class='alert alert-danger'>Error</div>");
                } else {
                    $celda.prepend("<div role='alert' class='alert alert-success'>Ok</div>");
                }
                console.log(data);
            });
    });

    $(".guardarPrecio").click(function() {
        $celda = $(this).parent();
        $codigo = $(this).parent().prev().prev().prev().prev().text();
        $precio = $(this).prev().prev().val();
        console.log($codigo + " " + $precio);
        $.post(
            "actualizarPrecio.php", {
                codigo: $codigo,
                precio: $precio
            },
            function(data, status) {
                if (!data || data === "") {
                    $celda.prepend("<div role='alert' class='alert alert-danger'>Error</div>");
                } else {
                    $celda.prepend("<div role='alert' class='alert alert-success'>Ok</div>");
                }
                console.log(data);
            });
    });

    $(".guardarStock").click(function() {
        $celda = $(this).parent();
        $codigo = $(this).parent().prev().prev().prev().prev().prev().text();
        $stock = Math.trunc($(this).prev().prev().val());
        console.log($codigo + " " + $stock);
        $.post(
            "actualizarStock.php", {
                codigo: $codigo,
                stock: $stock
            },
            function(data, status) {
                if (!data || data === "") {
                    $celda.prepend("<div role='alert' class='alert alert-danger'>Error</div>");
                } else {
                    $celda.prepend("<div role='alert' class='alert alert-success'>Ok</div>");
                }
                console.log(data);
            });
    });
</script>

</html>