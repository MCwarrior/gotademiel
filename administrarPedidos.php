<?php

require_once "privado/autoloader.php";

if (session_status() != PHP_SESSION_ACTIVE)
    session_start();

if (!isset($_SESSION['id'])) {
    Mensajes::establecerMensaje("error", "Necesitas iniciar sesión");
    header("location: login.php");
    exit;
}

if ($_SESSION['tipo_usuario'] != 'admin') {
    Mensajes::establecerMensaje("error", "Necesitas ser administrador");
    header("location: index.php");
    exit;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>La gota de miel</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/estilos.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
</head>

<body>
    <header>
        <img id="logoHeader" src="img/logo.png">
        <h1 id="tituloHeader">La Gota De Miel</h1>
        <div id="botonesHeader">
            <?php
            if (session_status() != PHP_SESSION_ACTIVE)
                session_start();
            if (isset($_SESSION["id"])) {
                print("Bienvenido " . $_SESSION["nombre"] . " |");
                print("<a href='perfil.php' id='headerMiPerfil'>Mi Perfil</a> |");
                print("<a href='logout.php' id='headerMiPerfil'>Cerrar sesión</a> |");
            } else {
                print("<a href='login.php' id='headerLogin'>Iniciar sesión</a> | <a href='registro.php' id='indexRegistro'>Registrarse</a>");
            }
            ?>
        </div>
        <div class="scrollmenu" id="menuHeader">
            <a href="index.php">Inicio</a>
            <a href="categorias.php">Categorías</a>
            <a href="listaProductos.php">Productos</a>
            <a href="#about">Contacto</a>
            <a href="carrito.php">Carrito</a>
            <a href="pedidos.php">Pedidos</a>
            <?php if($_SESSION['tipo_usuario'] == 'admin') echo "<a href='administrarProductos.php'>Admin. Productos</a>"; ?>
        </div>
    </header>
    <main>
        <h1>Mis Pedidos</h1>
        <table id="misPedidos">
            <tr>
                <th>Folio</th>
                <th>Fecha</th>
                <th>Estado</th>
                <th>Actualizar estado</th>
                <th>Total</th>
            </tr>
            <?php

            $conexion = Bd::obtenerConexion();
            $sql = "SELECT folio, fecha, estado, total from pedidos order by folio asc";

            $res = $conexion->query($sql);

            while ($datos = $res->fetch_assoc()) {
                ?>
                <tr>
                    <td><?= $datos['folio'] ?></td>
                    <td><?= $datos['fecha'] ?></td>
                    <td>
                        <select><?= $datos['estado'] ?>
                            <option value="aceptado" <?php if ($datos['estado'] == 'aceptado') echo "selected"; ?>>Aceptado</option>
                            <option value="enviado" <?php if ($datos['estado'] == 'enviado') echo "selected"; ?>>Enviado</option>
                            <option value="recibido" <?php if ($datos['estado'] == 'recibido') echo "selected"; ?>>Recibido</option>
                            <option value="cancelado" <?php if ($datos['estado'] == 'cancelado') echo "selected"; ?>>Cancelado</option>
                        </select>
                    </td>
                    <td><button type="button" id="guardarEstado">Actualizar</button></td>
                    <td><?= $datos['total'] ?></td>
                </tr>
            <?php
        }
        ?>
        </table>
    </main>
    <footer>
        Av. 20 de noviembre #51 (627) 522-1217 || Av. independencia #85 (627) 523-3520
    </footer>
</body>

<script>
    $("#guardarEstado").click(function() {
        $folio = $(this).parent().prev().prev().prev().text();
        $estado = $(this).parent().prev().children().val();
        console.log($folio + " " + $estado);
        $.post(
            "actualizarEstado.php", {
                folio: $folio,
                estado: $estado
            },
            function(data, status) {
                if(!data || data === ""){
                    $("main").prepend("<div role='alert' class='alert alert-danger'>No se pudo actualizar el estado</div>");
                }else{
                    $("main").prepend("<div role='alert' class='alert alert-success'>Se actualizó y notificó al usuario al correo " + data + "</div>");
                }
                console.log(data);
            });
    });
</script>

</html>