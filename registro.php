<?php
    require_once "privado/autoloader.php";
    $error = Mensajes::obtenerMensaje("error");
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>La gota de miel</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/estilos.css">
</head>
<body>
    <div class="logo">
       <a href="index.php"> <img src="img/logo.png" alt="Logo"></a>
    </div>
    <form action="procesarRegistro.php" method="POST" id="registro">
        <h1>Registro</h1>
        <?php                           
            if(isset($error)){
                echo "<div class='alert alert-danger' role='alert'>$error</div>";                
            }
        ?>
        <label for="nombre">Nombre</label>
        <input type="text" name="nombre">
        <label for="correo">Correo</label>
        <input type="email" name="correo">
        <label for="contrasenia">Contraseña</label>
        <input type="password" name="contrasenia">
        <label for="fechaNacimiento">Fecha de nacimiento</label>
        <input type="date" name="fechaNacimiento">
        <label for="direccion">Dirección</label>
        <input type="text" name="direccion">
        <label for="telefono">Telefono</label>
        <input type="number" maxlenght="10" name="telefono">
        <input type="submit" value="Registro" id="btnRegistrar">
    </form>
</body>
</html>